let parseString = require('xml2js').parseString;

let dataurls = {
  images: "http://thecatapi.com/api/images/get?format=xml&results_per_page=25",
  facts: "https://cors-anywhere.herokuapp.com/https://catfact.ninja/facts?limit=25"
}

function fetchData(url, _dataType, callback) {
  // performs api calls sending the required authentication headers
  const headers = {
    "Origin": "localhost"
  }

  const dataType = _dataType;

  return fetch(url, {
      method: "GET",
      headers
    })
    .then(res => {
      if (!res.ok) {
        return Error(res.statusText);
      }
      return res;
    })
    .then((res) => {

      switch (dataType) {
        case "xml":
          return res.text();
        default:
          return res.json();
      }

    })
    .then((_data) => {

      let retData;

      switch (dataType) {
        case "xml":
          parseString(_data, function(err, result) {
            retData = result;
          });
          break;
        default:
          retData = _data;
      }

      return callback(null, retData);

    })
    .catch(err => {
      return callback(err);
    })
}

export {
  dataurls,
  fetchData
};