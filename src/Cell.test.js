import React from "react";
import Cell from "./Cell";

let focusedView = {
  active: false,
  cat: {}
};

let cat = {
  img: "",
  id: "",
  fact: "",
  sortWord: "",
  favorite: false,
  focused: false
};

let setFavorite = (data) => {
  return data;
}

let setFocusedView = () => {
  return cat;
};

it('renders without crashing', () => {

  const wrapper = shallow(<Cell cat={cat} key={"1"} focusedView={focusedView}  setFavorite={setFavorite} setFocusedView={setFocusedView}/>);
  expect(wrapper).toMatchSnapshot();

});

it("calls setFocusedView on hover", () => {

  const spy = sinon.spy(setFocusedView);

  const wrapper = mount(<Cell cat={cat} key={"1"} focusedView={focusedView}  setFavorite={setFavorite} setFocusedView={spy}/>);

  wrapper.find("div").first().simulate("mouseenter");

  expect(spy.calledOnce).toBe(true);

  expect(spy.returnValues[0]).toBe(cat);

});

it("gets favorite call from star", () => {

  const cellSpy = sinon.spy(setFavorite);

  const cell = mount(<Cell cat={cat} key={"1"} focusedView={focusedView}  setFavorite={cellSpy} setFocusedView={setFocusedView}/>);

  expect(cell.find(".star")).toHaveLength(1);

  cell.find(".star").simulate("click");

  expect(cellSpy.calledOnce).toBe(true);

});