import React from 'react';
import Star from "./Star";

let classNames = require("classnames");

class Cell extends React.Component {

  constructor(props) {

    super(props);

    this.handleFavorite = this.handleFavorite.bind(this);

    this.focusedView = this.focusedView.bind(this);

  }

  handleFavorite(data) {
    this.props.setFavorite(data);
  }

  focusedView(data) {
    this.props.setFocusedView(data);
  }

  render(props) {

    let focusViewClass = classNames({
      cell: true,
      "focus-view": this.props.focusedView.active,
      "focus-view--active": this.props.focusedView.cat.id === this.props.cat.id
    })

    return (

      <div className={ focusViewClass } key={this.props.cat.id} onMouseEnter={ () => this.focusedView(this.props.cat) }>
            <div className="cell__image">
              <img src={this.props.cat.img} alt="cat image"/>
              <Star favorite={this.props.cat.favorite} dataid={this.props.cat.id} handleFavorite={this.handleFavorite}/>
            </div>
            <div className="cell__info">
              
                <p>{this.props.cat.fact}</p>
              
            </div>
          </div>
    )
  }

}

export default Cell;