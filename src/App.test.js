import React from "react";
import App from './App';

it('renders without crashing', () => {
  const wrapper = render(<App />);
  expect(wrapper).toMatchSnapshot();

});