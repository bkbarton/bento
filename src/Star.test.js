import React from "react";
import Star from "./Star";

let handleFavorite = () => {
  return true;
};

it('renders without crashing', () => {

  const wrapper = shallow(<Star favorite={"true"} dataid={1} handleFavorite={handleFavorite} />);
  expect(wrapper).toMatchSnapshot();

});

it("calls handleChane on click", () => {

  const spy = sinon.spy(handleFavorite);

  const wrapper = mount(<Star favorite={"true"} dataid={1} handleFavorite={spy} />);

  wrapper.find(".star").simulate("click");

  expect(spy.calledOnce).toBe(true);
  
});