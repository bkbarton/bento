import React from 'react';

class Star extends React.Component {
  constructor(props) {

    super(props);
    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(data) {
    this.props.handleFavorite(data);
  }

  render(props) {
    return (
      <div className="favorite" >
          <svg height="25" width="23" className={ "star " + (this.props.favorite ? "active" : "") } data-id={this.props.dataid} onClick={(ele)=>{this.handleChange(this.props.dataid)}}>
            <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
          </svg>
        </div>
    );
  }

}

export default Star;