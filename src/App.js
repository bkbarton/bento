import React, {
  Component
} from 'react';
import './App.css';
import {
  fetchData,
  dataurls
} from "./fetchMethod.js";
import Cell from "./Cell";


function Cat(img, id, fact, isFavorite, isFocused) {
  this.img = img || "";
  this.id = id || "";
  this.fact = fact || "";
  this.sortWord = lastWord(fact) || "";
  this.favorite = isFavorite || false;
  this.focused = isFocused || false;
}

function lastWord(text) {
  return text.split(" ").pop().replace(/[^a-zA-Z ]/g, "");
}

class App extends Component {

  constructor(props) {

    super(props);

    this.state = {
      cats: [],
      favoritesView: false,
      sort: {
        asc: true,
        active: false
      },
      focusedView: {
        active: false,
        cat: {}
      }
    }

    this.setFavorite = this.setFavorite.bind(this);
    this.toggleFavorite = this.toggleFavorite.bind(this);
    this.toggleSort = this.toggleSort.bind(this);
    this.toggleFocusView = this.toggleFocusView.bind(this);
    this.setFocusedView = this.setFocusedView.bind(this);
  }

  setFavorite(eleID) {

    this.setState({
      cats: this.state.cats.map(
        (cat) => {
          if (cat.id === eleID) {
            cat = { ...cat,
              favorite: !cat.favorite
            };
          }
          return cat
        }
      )
    });

  }

  toggleFavorite() {

    this.setState({
      favoritesView: !this.state.favoritesView
    });

  }

  toggleSort(ascFlag) {

    let sortActive = (this.state.sort.asc === ascFlag) ? false : true;

    this.setState({
      sort: Object.assign({}, this.state.sort, {
        active: sortActive,
        asc: ascFlag
      }),
      cats: this.state.cats.sort((a, b) => {
        if (ascFlag === true) {
          return a.sortWord.localeCompare(b.sortWord);
        }

        return b.sortWord.localeCompare(a.sortWord);
      })
    })
  }

  toggleFocusView(id) {
    this.setState({
      focusedView: Object.assign({}, this.state.focusedView, {
        active: !this.state.focusedView.active
      })
    });
  }

  setFocusedView(thisCat) {

    if (this.state.focusedView.active) {
      this.setState({
        focusedView: Object.assign({}, this.state.focusedView, {
          cat: thisCat
        })
      });
    }

  }

  componentDidMount() {

    let imgs = new Promise(
      (resolve, reject) => {
        fetchData(dataurls.images, 'xml', (err, data) => {

          if (err) {
            reject(err);
          } else resolve(data.response.data[0].images[0].image);

        })
      }
    );

    let facts = new Promise(
      (resolve, reject) => {
        fetchData(dataurls.facts, null, (err, data) => {

          if (err) {
            reject(err);
          } else resolve(data.data);

        })
      }
    );

    Promise.all([imgs, facts])
      .then((res) => {

        var cats = res[0].map((ele, i) => {
          return new Cat(ele.url[0], ele.id[0], res[1][i].fact);
        });

        this.setState({
          cats: cats
        });

      }).catch((err) => {
        throw err;
      });

  }

  render() {

    let catsToShow = (this.state.favoritesView) ? this.state.cats.filter((cat) => {
      return cat.favorite === true;
    }) : this.state.cats;
    let Cats = catsToShow.map((cat, i) => {
      return <Cell key={cat.id} cat={cat} focusedView={this.state.focusedView} setFocusedView={this.setFocusedView} setFavorite={this.setFavorite}/>
    });
    return (
      <div className="App">
        <nav  className="nav">
          <span className="logo"><b>Caterest</b></span>
          <button className={"btn " + (this.state.favoritesView ? 'btn--active' : '')} onClick={this.toggleFavorite}>Favorites Miew</button>
          <button className={"btn " + (this.state.focusedView.active ? 'btn--active' : '')} onClick={this.toggleFocusView}>Focus Miew</button>
          <span className="sorting">
            <button className={"btn " + ( this.state.sort.active === true && this.state.sort.asc === true  ? 'btn--active' : '' )} onClick={ () => this.toggleSort(true) } >Sort A mew Z</button>
            <button className={"btn " + ( this.state.sort.active === true && this.state.sort.asc === false  ? 'btn--active' : '' )} onClick={() => this.toggleSort(false)}>Sort Z mew A</button>
          </span>
        </nav>

        { 
          this.state.cats.length != 25 &&
          <div className="gen-info">Welcome to Caterest! <br/>We are loading some something purrty, just for you.</div>
        }

        {
          this.state.cats.length === 25 &&
          <section className="cells">
            { Cats }
          </section>
        }

        {
          this.state.favoritesView && catsToShow.length === 0 &&
          <div className="gen-info">Meow meow... <br/>Mew haven't set any favorites yet.<br/> Go ahead, click the star on a kitty's photo & on come back.</div>
        }
      </div>
    );
  }
}





export default App;